import { UserService, AuthenticationError } from '../services/user.service'
import { TokenService } from '../services/storage.service'
import router from '../router/index'

const state = {
  authenticating: false,
  accessToken: TokenService.getToken(),
  AuthenticationErrorCode: 0,
  AuthenticationError: ''
}

const getters = {
  loggedIn: (state) => {
    return state.accessToken ? true : false
  },

  AuthenticationErrorCode: (state) => {
    return state.AuthenticationErrorCode
  },

  AuthenticationError: (state) => {
    return state.AuthenticationError
  },

  authenticating: (state) => {
    return state.authenticating
  }
}

const actions = {
  async login({ commit }, { email, password }) {
    commit('loginRequest');

    try {
      const token = await UserService.login(email, password);
      commit('loginSuccess', token)

      //Redirect the user to the page he first tried to visit or to the home view
      router.push(router.history.current.query.redirect || '/')

      return true
    } catch (e) {
      if (e instanceof AuthenticationError) {
        commit('loginError', {errorCode: e.errorCode, errorMessage: e.message})
      }

      return false
    }
  },

  logout({ commit }) {
    UserService.logout()
    commit('logoutSuccess')
    router.push('/login')
  }
}

const mutations = {
  loginRequest(state) {
    state.authenticating = true
    state.AuthenticationError = ''
    state.AuthenticationErrorCode = 0
  },

  loginSuccess(state, accessToken) {
    state.accessToken = accessToken
    state.authenticating = false
  },

  loginError(state, {errorCode, errorMessage}) {
    state.authenticating = false
    state.AuthenticationErrorCode = errorCode
    state.AuthenticationError = errorMessage
  },

  logoutSuccess(state) {
    state.accessToken = ''
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
